package com.example.smartringtone;

import org.jraf.android.backport.switchwidget.Switch;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class HomeFragment extends Fragment {
	
	public static final String  AMBIENT_SWITCH = "ambient_switch";
	protected static final String SMART_GPS_SWITCH = "smart_gps_switch";
	protected static final String MY_LOCATIONS = "my_locations";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_home, container, false);
		Switch ambientSwitch = (Switch) v.findViewById(R.id.ambient_switch);
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		ambientSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				prefs.edit()
				.putBoolean(AMBIENT_SWITCH, isChecked)
				.commit();
				
				if (isChecked) {
					PollService.setServiceAlarm(getActivity(), true);
				} else {
					PollService.setServiceAlarm(getActivity(), false);
				}
			}
		});
		
		Switch gpsSwitch = (Switch) v.findViewById(R.id.smart_gps_switch);
		gpsSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				prefs.edit()
				.putBoolean(SMART_GPS_SWITCH, isChecked)
				.commit();
			}
		});
		
		Switch myLocations = (Switch) v.findViewById(R.id.my_locations_switch);
		myLocations.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				prefs.edit()
				.putBoolean(MY_LOCATIONS, isChecked)
				.commit();
				
			}
		});
		
		ambientSwitch.setChecked(prefs.getBoolean(AMBIENT_SWITCH, false));
		gpsSwitch.setChecked(prefs.getBoolean(SMART_GPS_SWITCH, false));
		myLocations.setChecked(prefs.getBoolean(MY_LOCATIONS, false));
		
		return v;
	}
}