package com.example.smartringtone;

import java.io.IOException;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PollService extends IntentService {
	
	private static final String POLL_SERVICE = "POLL_SERVICE";
	private static final int POLL_INTERVAL = 1000 * 60; // every minute 
	private static final double REFERENCE = 0.1;
	public PollService() {
		super(POLL_SERVICE);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			MediaRecorder recorder = setUpMic();
			double db = decibelNoiseLevel(recorder);
			setRingerVolume(db);
			stopRecorder(recorder);
		}
	}

	private void setRingerVolume(double db) {
		AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int max = audio.getStreamMaxVolume(AudioManager.STREAM_RING);
		int lowerBound = 20;
		boolean set = false;
		for (int i = 0; i <= max && !set; i++) {
			if (db < lowerBound) {
				audio.setStreamVolume(AudioManager.STREAM_RING, i, 0);
				set = true;
			}
			lowerBound += 10;
		}
		if (!set) {
			audio.setStreamVolume(AudioManager.STREAM_RING, max, 0);
		}
	}

	private void stopRecorder(MediaRecorder recorder) {
		recorder.stop();
		recorder.reset();
		recorder.release();
	}

	private double decibelNoiseLevel(MediaRecorder recorder) {
		int amplitude = recorder.getMaxAmplitude();
		while (amplitude == 0) {
			amplitude = recorder.getMaxAmplitude();
		}
		double db = 20 * Math.log10(amplitude / REFERENCE);
		return db;
	}

	private MediaRecorder setUpMic() {
		MediaRecorder recorder = new MediaRecorder();
		 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		 recorder.setAudioChannels(1);
		 recorder.setOutputFile("/dev/null");
		 recorder.setMaxDuration(10000);
		try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		recorder.start();
		return recorder;
	}
	
	public static void setServiceAlarm(Context context, boolean isOn) {
		Intent i = new Intent(context, PollService.class);
		PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
		
		AlarmManager alarmManager = (AlarmManager)
				context.getSystemService(Context.ALARM_SERVICE);
		
		if (isOn) {
			alarmManager.setRepeating(AlarmManager.RTC,
					System.currentTimeMillis(), POLL_INTERVAL, pi);
		} else {
			alarmManager.cancel(pi);
			pi.cancel();
		}
	}
	
	public static boolean isServiceAlarmOn(Context context) {
		Intent i = new Intent(context, PollService.class);
		PendingIntent pi = PendingIntent
				.getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
		return pi != null;
	}
}
